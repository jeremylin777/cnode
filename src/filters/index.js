//转换时间
import beijingTime from "../utils/beijingTime";

export default function (Vue) {
  Vue.filter("time", function (time) {
    // 保留原始的时间
    let result = beijingTime(time);
    //把分，时，天，周，半个月，一个月用毫秒表示
    let minute = 1000 * 60;
    let hour = minute * 60;
    let day = hour * 24;
    let week = day * 7;
    let halfamonth = day * 15;
    let month = day * 30;
    let year = day * 365;

    //获取当前时间毫秒
    let now = Date.now();

    // 截取转换下
    // time = time.substring(0, 18);

    // 转化成毫秒数
    let timestamp = new Date(time).getTime();
    console.log(timestamp, "timestamp");

    //时间差
    let diffValue = now - timestamp;

    // 超过当前时间,直接return
    if (diffValue < 0) {
      return result;
    }

    //计算时间差的分，时，天，周，月
    let minC = diffValue / minute;
    let hourC = diffValue / hour;
    let dayC = diffValue / day;
    let weekC = diffValue / week;
    let monthC = diffValue / month;
    let yearC = diffValue / year;
    if (yearC >= 1) {
      result = parseInt(yearC) + "年前";
    } else if (monthC >= 1 && monthC <= 12) {
      result = parseInt(monthC) + "月前";
    } else if (dayC >= 1 && dayC <= 30) {
      result = parseInt(dayC) + "天前";
    } else if (hourC >= 1 && hourC <= 24) {
      result = parseInt(hourC) + "小时前";
    } else if (minC >= 1 && minC <= 60) {
      result = parseInt(minC) + "分钟前";
    } else if (diffValue >= 0 && diffValue <= minute) {
      result = "刚刚";
    } else {
      // 时间太久
      // 全都是走它了
      result = time;
    }

    // 最后return出来
    return result;
  });
}
