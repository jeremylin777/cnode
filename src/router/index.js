import Vue from "vue";
import VueRouter from "vue-router";
import HomeView from "../views/home";

Vue.use(VueRouter);

export const routes = [
  {
    path: "/",
    name: "home",
    component: HomeView,
    meta: {
      til: "首页",
    },
  },
  {
    path: "/newser",
    name: "newser",
    meta: {
      til: "新手入门",
    },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ "../views/newser"),
  },
  {
    path: "/api",
    name: "api",
    meta: {
      til: "Api",
    },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ "../views/api"),
  },
  {
    path: "/about",
    name: "about",
    meta: {
      til: "关于",
    },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ "../views/about"),
  },
  {
    path: "/login",
    name: "login",
    meta: {
      til: "登陆",
    },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ "../views/login"),
  },

  {
    path: "/topic/:id",
    name: "topic",
    meta: {
      til: "加载中...",
    },
    hidden: true, //导航隐藏
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ "../views/topic"),
  },
  {
    path: "/author/:name",
    name: "author",
    hidden: true,
    meta: {
      til: "加载中..",
    },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ "../views/author"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
