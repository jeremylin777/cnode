import request from "@/utils/request";

// 获取主题列表
// tab page limit
export function getTopicList(params = {}) {
  return request({
    url: "/topics",
    method: "get",
    params,
  });
}

// 获取主题详情
export function getTopic(id) {
  return request({
    url: "/topic/" + id,
    method: "get",
  });
}

// 赞
/**
 *
 * 怎么传递的  要传递什么
 *
 */
export function setUp(id) {
  return request({
    url: `/reply/${id}/ups`,
    method: "post",
  });
}
//  收藏
export const collect = (topic_id) => {
  return request({
    url: "/topic_collect/collect",
    method: "post",
    data: {
      topic_id,
    },
  });
};

//  取消收藏
export const de_collect = (topic_id) => {
  return request({
    url: "/topic_collect/de_collect",
    method: "post",
    data: {
      topic_id,
    },
  });
};
//新建评论  对主题进行评论  对 评论进行评论
// data 可能有什么参数  { content: this.content } {content reply_id}
export function reply(topic_id, data) {
  return request({
    url: `/topic/${topic_id}/replies`,
    method: "post",
    data,
  });
}
