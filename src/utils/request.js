import axios from "axios";
import store from "../store";
const request = axios.create({
  baseURL: "https://cnodejs.org/api/v1", //前缀地址
});

// 请求拦截  --页面发送请求  --- 拦截器   --- 服务器  （路由全局钩子）

// 请求拦截

//

request.interceptors.request.use(
  function (config) {
    //获取token
    let token = store.state.user.token;

    // config.params = {...config.params,accesstoken:token}
    // config.data ={name:"zhangsan",accesstoken:token}
    if (config.method.toLocaleLowerCase() == "get") {
      config.params = { ...config.params, accesstoken: token };
    }
    if (config.method.toLocaleLowerCase() != "get") {
      config.data = { ...config.data, accesstoken: token };
    }
    // 在发送请求之前进行操作
    //
    return config;
  },
  function (error) {
    // 对请求错误进行操作
    return Promise.reject(error);
  }
);

//登陆不是一个

// 响应拦截器

export default request;
