import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import components from "./components";
import filters from "./filters";
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
//富文本编辑器
import Vue2Editor from "vue2-editor";

Vue.use(ElementUI).use(components).use(Vue2Editor).use(filters);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
