import Mark from "./Mark";
import List from "./List";
import Avatar from "./Avatar";
export default function (Vue) {
  Vue.component("Mark", Mark);

  Vue.component("List", List);

  Vue.component("Avatar", Avatar);
}
