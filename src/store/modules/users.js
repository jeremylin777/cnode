import axios from "axios";
import { Message } from "element-ui";
import router from "../../router";

// 设定一个初始值
const initState = () => ({
  author: {
    loginname: "",
    avatar_url: "", //请求  author
  },
  token: "", //两个必须
});

// 初始值
const state = initState();
//修改state
const mutations = {
  //commit
  SET_AUTHOR(state, author) {
    state.author = author;
  },
  SET_TOKEN(state, token) {
    state.token = token;
  },
};

const actions = {
  // userInfo 页面调用的账号和密码
  login(context, userInfo) {
    axios({
      url: "http://114.116.29.103:8081/users",
      method: "get",
      params: userInfo,
    }).then((res) => {
      // console.log(res, "login");
      if (res.data.length) {
        // 对象的解构赋值
        let { token, nickname, avatar_url } = res.data[0];
        //登陆成功
        Message({
          message: "登陆成功",
          type: "success",
          duration: 1500,
        });
        /**
         *   将数据存储 state
         *   跳转到首页
         */
        //
        context.commit("SET_AUTHOR", {
          loginname: nickname,
          avatar_url,
        });
        context.commit("SET_TOKEN", token);

        setTimeout(() => {
          router.push("/");
        }, 1500);
      } else {
        // 用户名或密码错误
        Message({
          message: "用户名或密码错误",
          type: "error",
          duration: 1500,
        });
      }
    });
  },
};

export default {
  namespaced: true, //xi
  state,
  mutations,
  actions,
};
