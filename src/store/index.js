import Vue from "vue";
import Vuex from "vuex";
import user from "./modules/users";
import Per from "vuex-persistedstate";
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    tabs: {
      ask: "问答",
      share: "分享",
      job: "招聘",
    },
    them: {
      loginname: "",
      avatar_url: "",
    },
  },
  getters: {
    isLogin(state) {
      return state.user.token != "";
    },
  },
  mutations: {
    SET_THEM(state, payload) {
      state.them = payload;
    },
  },
  actions: {},
  modules: {
    user,
  },
  plugins: [
    Per({
      storage: window.sessionStorage,
      key: "sy2201", // 默认key vuex
      paths: ["user"], //默认都存储
    }),
  ],
});
